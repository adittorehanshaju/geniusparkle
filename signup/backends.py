from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model

UserModel = get_user_model()


class UserBackend(ModelBackend):

    def authenticate(self, request, email=None, password=None, **kwargs):
        if email is None:
            email = kwargs.get(UserModel.USERNAME_FIELD)
        try:
            case_insensitive_email_field = '{}__iexact'.format(UserModel.USERNAME_FIELD)
            user = UserModel._default_manager.get(**{case_insensitive_email_field: email})
        except UserModel.DoesNotExist:
            UserModel.set_password(password)

        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                return user
