from django import forms
from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.forms import ReadOnlyPasswordHashField, UserCreationForm

from .validations import normalize_email

User = get_user_model()


class RegisterForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.pop("autofocus", None)

    username = None
    password1 = None
    password2 = None

    class Meta:
        model = User
        fields = [
            'name',
            'email',
            'password'
        ]

        widgets = {
            'name': forms.TextInput(attrs={
                'minln': 5,
                'maxln': 100,
                'pattern': r"^([a-zA-Z]+)([.]?[\s]?[a-zA-Z]+)*$",
                'required': True

            }),
            'email': forms.EmailInput(attrs={
                'pattern': r"^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$",
                'required': True
            }),
            'password': forms.TextInput(attrs={
                'name': 'password',
                'minln': 8,
                'maxln': 100,
                'required': True
            })
        }

        def clean_password(self):
            password = self.cleaned_data.get('password')
            if len(password) < 8:
                raise ValueError("pass_error")
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:

                # Method inherited from BaseForm
                self.add_error('password', error)
            return password

    def save(self, commit=True):
        user = forms.ModelForm.save(self, commit=False)
        if User.objects.filter(email=normalize_email(self.cleaned_data['email'])).exists():
            raise ValueError('Email address is already registered by another user.')
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

    def _post_clean(self):
        forms.ModelForm._post_clean(self)
        # Validate the password after self.instance is updated with form data
        # by super().

    def save(self, commit=True):
        user = forms.ModelForm.save(self, commit=True)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserAdminCreationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    password_2 = forms.CharField(
        label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email']

    def clean(self):
        '''
        Verify both passwords match.
        '''
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_2 = cleaned_data.get("password_2")
        if password is not None and password != password_2:
            self.add_error("password_2", "Your passwords must match")
        return cleaned_data

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ['email', 'password', 'is_active', 'admin']

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]
