from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models

from .validations import normalize_email


class UserManager(BaseUserManager):
    def create_user(self, email, password, name):
        """
        Creates and saves a User with the given email, password, name
        """
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email=self.normalize_email(normalize_email(email)),
            name=name
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, password, name):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email,
            password,
            name
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, name):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password,
            name
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=100)
    dobDay = models.IntegerField(default=None, blank=True, null=True)
    dobMonth = models.CharField(default=None, blank=True, null=True, max_length=9)
    dobYear = models.IntegerField(default=None, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = [
        'name'
    ]

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin


# TODO: need to encrypt
class VerificationNumber(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    number = models.CharField(max_length=200)
    date = models.DateTimeField('verification date')
