from django.contrib.auth.tokens import PasswordResetTokenGenerator
from six import text_type


class TokenGenerator(PasswordResetTokenGenerator):

    def _make_hash_value(self, user, timestamp):
        link_token = text_type(user.is_active) + text_type(user.id) + text_type(timestamp)
        return link_token
