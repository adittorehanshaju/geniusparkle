from django.urls import path
from . import views


app_name = 'signup'
urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('Sign-Up/', views.signup, name='signup'),
    path('Sign-In/', views.log_in, name='login'),
    path('Log-Out/', views.log_out, name='logout'),
    path('activate/<uidb64>/<token>/', views.VerificationLinkView.as_view(), name='verify_email_link'),
    path('activate/<uidb64>/', views.verify_email, name='verify_number_link'),
    path('resend/<uidb64>/', views.resend, name='resend_number'),

]
