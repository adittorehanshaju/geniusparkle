import re


class PassError(ValueError):
    pass


class NameError(ValueError):
    pass


class EmailError(ValueError):
    pass


pass_re = re.compile("^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$")


# Raise Password Error if a string does not include atleast one letter, number and special character
# also length less then 8

def check_password(password):
    if len(password) < 8:
        raise PassError("Password Error")
    num = any(chr.isdigit() for chr in password)
    if not num:
        raise PassError("Password Error")
    if not re.search('[a-z]', password):
        raise PassError("Password Error")
    if not re.search('[A-Z]', password):
        raise PassError("Password Error")
    if not re.search('[0-9]', password):
        raise PassError("Password Error")


name_re = re.compile("^([a-zA-Z]+)([.]?[\s]?[a-zA-Z]+)*$")


def check_name(name):
    name_len = len(name)
    if name_len < 5 or name_len > 100:
        raise NameError("Name Error")
    if not name_re.match(name):
        raise NameError("Name Error")


email_re = re.compile(
    r"^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$")


def check_email(email):
    if not email_re.match(email):
        raise EmailError("Email Error")


def normalize_email(email):
    normalized_email = str(email).lower()
    return normalized_email
