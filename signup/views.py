import random

from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from django.views import View
from django.utils.encoding import force_bytes, force_text, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from django.utils import timezone
from .models import VerificationNumber

from .forms import RegisterForm
from . import validations
from .tokens import TokenGenerator

User = get_user_model()


@login_required(login_url='signup:login')
def homepage(request):
    return render(request, 'signup/homepage.html')


def send_email(domain, uidb64):
    user_id = force_text(urlsafe_base64_decode(uidb64))
    if user_id is not None and User.objects.filter(id=user_id).exists():
        user = User.objects.get(id=user_id)
        link_token = TokenGenerator().make_token(user)
        verification_number = random.randint(10000, 100000)
        link = reverse('signup:verify_email_link', kwargs={'uidb64': uidb64, 'token': link_token})
        activate_url = 'http://' + str(domain) + str(link)
        mail = user.email
        email_body = "Use this number: " + str(
            verification_number) + "\nPlease Verify your email by click the link:\n" + activate_url

        email = EmailMessage(
            'Verify Email',
            email_body,
            'noreply@email.com',
            [mail],

        )
        email.send(fail_silently=False)

        number_token = urlsafe_base64_encode(force_bytes(verification_number))
        VerificationNumber(user=user,
                           number=number_token,
                           date=timezone.now()
                           ).save()


def signup(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            form = RegisterForm(request.POST)
            name = request.POST['name']
            password = request.POST['password']
            email = validations.normalize_email(request.POST['email'])
            try:
                validations.check_name(name)
                validations.check_email(email)
                validations.check_password(password)
            except validations.NameError:
                return render(request, 'signup/index.html', {'form': RegisterForm(), 'name_error': True})
            except validations.EmailError:
                return render(request, 'signup/index.html', {'form': RegisterForm(), 'email_error': True})
            except validations.PassError:
                return render(request, 'signup/index.html', {'form': RegisterForm(), 'pass_error': True})
            if form.is_valid():
                try:
                    name = form.cleaned_data['name']
                    email = form.cleaned_data['email']
                    password = form.cleaned_data['password']
                    user = User.objects.create_user(name=name, email=email, password=password)
                    user.is_active = False
                    user.save()
                    uidb64 = urlsafe_base64_encode(force_bytes(user.id))
                    send_email(get_current_site(request), uidb64=uidb64)
                    return redirect(reverse('signup:verify_number_link', kwargs={'uidb64': uidb64}))
                except ValueError:
                    return render(request, 'signup/index.html', {'form': RegisterForm(), 'user_exists': True})
                
            else:
                render(request, 'signup/index.html', {'form': form})
        return render(request, 'signup/index.html', {'form': RegisterForm()})

    else:
        homepage(request)


class VerificationLinkView(View):
    def get(self, request, uidb64, token):
        try:
            user_id = force_text(urlsafe_base64_decode(uidb64))
            user = None
            if User.objects.filter(id=user_id).exists():
                user = User.objects.get(id=user_id)
            if not TokenGenerator().check_token(user=user, token=token):
                return redirect('signup:login')
            if user is not None and user.is_active:
                return redirect('signup:login')
            user.is_active = True
            user.save()
            # TODO: Show success message at the client side
        except Exception as e:
            print(e)

        return redirect('signup:login')


def verify_email(request, uidb64):
    if request.method == 'POST':
        try:
            user_id = force_text(urlsafe_base64_decode(uidb64))
            if user_id is not None and User.objects.filter(id=user_id).exists():
                user = User.objects.get(id=user_id)
                if user is not None and user.is_active:
                    return redirect('signup:login')

                count = user.verificationnumber_set.count()
                if count > 0:
                    # Always gets the last verification number
                    number_model = user.verificationnumber_set.all()[count - 1]
                    verification_number = force_text(urlsafe_base64_decode(number_model.number))
                    number = int(
                        request.POST['digit1'] + request.POST['digit2'] + request.POST['digit3'] + request.POST[
                            'digit4'] + request.POST['digit5'])
                    verification_number = int(verification_number)
                    if verification_number == number:
                        user.is_active = True
                        user.save()
                        return render(request, 'signup/index.html')
                    else:
                        return redirect('signup:verify_number_link' + '?message=Wrong number please try again')
            # Todo: need to show some error messages
            return redirect('signup:signup')
            # TODO: Show success message at the client side
        except Exception as e:
            return render(request, 'signup/index.html')
    else:
        return render(request, 'signup/verify.html', {'uidb64': uidb64})


# When resend button is clickedSend email with link and number if it is a get request
def resend(request, uidb64):
    try:
        # user_id in safe url form
        user_id = force_text(urlsafe_base64_decode(uidb64))
        # If user exists
        if user_id is not None and User.objects.filter(id=user_id).exists():
            user = User.objects.get(id=user_id)
            if user is not None and user.is_active:
                # User is already registered
                return redirect('signup:login')
            send_email(get_current_site(request), uidb64=uidb64)
            count = user.verificationnumber_set.count()
            if count > 0:
                return render(request, 'signup/verify.html', {'uidb64': uidb64})
        # Todo: need to show some error messages
        return redirect('signup:signup')

        # TODO: Show success message at the client side
    except Exception as e:
        return redirect(reverse('signup:signup'))


def log_in(request):
    # If user is not logged in
    if not request.user.is_authenticated:
        # If user submit a post request
        user = None
        if request.method == 'POST':
            try:
                normalized_email = validations.normalize_email(request.POST['email'])
                user = User.objects.get(email=normalized_email)
            except User.DoesNotExist:
                render(request, 'signup/index.html', {'invalid_email_or_password': True})
            if user is not None:
                email = user.email
                password = request.POST.get('password')
                if password is not None:
                    user = authenticate(request, email=email, password=password)
                    if user is not None:
                        # if the user is verified
                        if user.is_active:
                            login(request, user)
                            return HttpResponseRedirect(reverse('signup:homepage'))
                        # User needs to verify email
                        else:
                            return render(request, 'signup/verify.html')

            return render(request, 'signup/index.html', {'invalid_email_or_password': True})

        # If user submit a get request
        else:
            return render(request, 'signup/index.html', {'invalid_email_or_password': False})
    # If user is logged in
    else:
        return HttpResponseRedirect(reverse('signup:homepage'))


def log_out(request):
    logout(request)
    return HttpResponseRedirect(reverse('signup:login'))
